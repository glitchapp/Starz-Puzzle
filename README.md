# Starz Puzzle (fork)

<img src="screenshot.webp" width=75% height=75%>


I'm not the original developer of this game, the original developer is "yonaba" and the original repository can be found here: https://github.com/Yonaba/Starz-Puzzle/

I modified the code to render the background and tiles at higher resolution and replaced most of the assets.

I also added the music tracks "Puzzle / Reflexion Loop + Opening Theme" by Lauromine (https://opengameart.org/content/puzzle-reflexion-loop-opening-theme).

# Starz Puzzle
A small Lua puzzle game written for the sake of LÖVE (0.8.0)

![game title](https://github.com/Yonaba/Starz-Puzzle/raw/master/screenshots/s1.png)
![in game screenshot 1](https://github.com/Yonaba/Starz-Puzzle/raw/master/screenshots/s2.png)
![in game screenshot 2](https://github.com/Yonaba/Starz-Puzzle/raw/master/screenshots/s3.png)
![in game screenshot 3](https://github.com/Yonaba/Starz-Puzzle/raw/master/screenshots/s4.png)

##Gameplay

Starz Puzzle is a strategy game where you have a limited amount of time to collect all the stars on a given level.<br/>
Some blocks will hinder you. Make smart moves to clear your path.<br/>

Warning! You can only push blocks, not pull them. <br/>
The longer it takes you to collect stars, the lower the points you are getting. <br/>
Hurry up, clock is ticking! <br/>

##License
Copyright (c) 2012 Roland Yonaba<br/>
The source code of this game MIT Licensed.

